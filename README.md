# pyProGA

**pyProGA - _python Protein Graph Analyser_** is a PyMOL plugin that allows the creation and analysis of protein residue network (PRN) models from various input data.

Create PRNs based on the proteins geometry ("distance" based) D-PRN, or PRNs based on pair interaction energies (PIEs); PIE-PRN. PIE-PRNs can be based on fragment molecular orbital (FMO) or force filed (FF) calculations.

The publication: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0255167

The compressed directory **pyProGA_x.zip** (x = release / version) is the plugin that is to be installed in PyMOL. Please **do** read the INSTALL.txt file for more information.

The directory **EXAMPLES** contains some files to teach you the use of this plugin.

If you have constructive comments or find some issues, please do let me know.

Good luck, and thanks' for using my code!

___________________
**Latest release: 1.2.6**
Full release history with bug fixes and feature additions is documented in the file RELEASE_HISTROY.txt
