TIM barrel 5BVL, PDB database: https://www.rcsb.org/structure/5BVL
Huang, P.S., Feldmeier, K., Parmeggiani, F., Fernandez Velasco, D.A., Hocker, B., Baker, D., (2016) Nat Chem Biol 12: 29-34
 
The structure is missing the residue ALA-17. It was not added,
by which we have formally two subsystems: helix "A" (res 1 - 16)
and the rest "B" (res 18 - 183). The file names reflect this.

Calculations were done in GAMESS & FMO vers. 5.4 using DFTB with the 3ob-3-1 parameter set.
